import React from 'react';
import './App.less';
import Menu from './Menu';
import Search from './Search';
import Related from './Related'

const App = () => {
  return (
    <div className='App'>
      <Menu />
      <Search />
      <Related />
    </div>
  );
};

export default App;