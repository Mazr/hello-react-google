import React from 'react';
import './Input.less';
import {MdSearch} from 'react-icons/md';
import {MdSettingsVoice} from 'react-icons/md';

const Input = () => {
  return (
    <div className='input'>
      <MdSearch className = 'md-search' />
      <input/>
      <MdSettingsVoice className = 'md-setting-voice'/>
    </div>
  );
};

export default Input;