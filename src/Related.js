import React from "react";
import './Related.less';

const Related = () => {
  return (
    <div className={'related'}>
      <section className={'location'}>
        <label >Hong Kong</label>
      </section>
      <section className={'footer'}>
        <nav className={"nav-left"}>
          <ul>
            <li><a href={"https://www.google.com/intl/en_hk/ads/?subid=ww-ww-et-g-awa-a-g_" +
            "hpafoot1_1!o2&utm_source=google.com&utm_medium=referral&utm_campaign=google_hpafooter&fg=1"}>
              Advertising
            </a></li>
            <li><a href={"https://www.google.com/intl/en_hk/ads/?subid=ww-ww-et-g-awa-a-g_" +
            "hpafoot1_1!o2&utm_source=google.com&utm_medium=referral&utm_campaign=google_hpafooter&fg=1"}>
              Business
            </a></li>
            <li><a href={"https://www.google.com/intl/en_hk/ads/?subid=ww-ww-et-g-awa-a-g_" +
            "hpafoot1_1!o2&utm_source=google.com&utm_medium=referral&utm_campaign=google_hpafooter&fg=1"}>
              About
            </a></li>
            <li><a href={"https://www.google.com/intl/en_hk/ads/?subid=ww-ww-et-g-awa-a-g_" +
            "hpafoot1_1!o2&utm_source=google.com&utm_medium=referral&utm_campaign=google_hpafooter&fg=1"}>
              How Search works
            </a></li>
          </ul>
        </nav>
        <nav className={"nav-right"}>
          <ul>
            <li> <a href={"https://policies.google.com/privacy?fg=1"}>
              Privacy
            </a></li>
            <li><a href={"https://www.google.com/intl/en_hk/ads/?subid=ww-ww-et-g-awa-a-g_" +
            "hpafoot1_1!o2&utm_source=google.com&utm_medium=referral&utm_campaign=google_hpafooter&fg=1"}>
              Terms
            </a></li>
            <li><a href={"https://www.google.com/intl/en_hk/ads/?subid=ww-ww-et-g-awa-a-g_" +
            "hpafoot1_1!o2&utm_source=google.com&utm_medium=referral&utm_campaign=google_hpafooter&fg=1"}>
              Setting
            </a></li>
          </ul>
        </nav>
      </section>
    </div>
  );
};

export default Related;