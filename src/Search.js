import React from 'react';
import './Search.less';
import Input from './Input';


const Search = () => {
  return (
    <section className='search'>
      <header>
        <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
             alt="Google Logo"/>
        <Input/>
      </header>
      <section>
        <button>Google Search</button>
        <button>I'm Feeling Lucky</button>
      </section>
      <footer>
        <p>
          Google offered in:
          <a href={"https://www.google.com/setprefs?sig=0_66KLJBoLf7YifYNPgEc369NReUY%3" +
               "D&hl=zh-TW&source=homepage&sa=X&ved=0ahUKEwjx_7OMh-TkAhUWIIgKHRO6CXUQ2ZgBCA8"}>
            中文（繁體）
          </a>
          <a href={"https://www.google.com/setprefs?sig=0_66KLJBoLf7YifYNPgEc369NReUY%3D&hl=" +
          "zh-CN&source=homepage&sa=X&ved=0ahUKEwjsh9Orh-TkAhVFfd4KHezZDvoQ2ZgBCBA"}>
            中文(简体)
          </a>
        </p>
      </footer>
    </section>
  );
};

export default Search;